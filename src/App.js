import './App.css';
import React, { useState } from 'react';
import axios from 'axios'
import ReactDOM from 'react-dom';

const testData = [
  {name: "Dan Abramov", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook"},
  {name: "Sophie Alpert", avatar_url: "https://avatars2.githubusercontent.com/u/6820?v=4", company: "Humu"},
  {name: "Sebastian Markbåge", avatar_url: "https://avatars2.githubusercontent.com/u/63648?v=4", company: "Facebook"},
];

const CardList = (props) => (
  <div className="CardList">
    {props.profiles.map(profile => <Card key={profile.avatar_url} {...profile} />)}
  </div>
);

const Card = (props) => {
    const profile = props;
    return (
      <div className="profile">
        <img scr={profile.avatar_url} />
        <div className="profile__info">
          <div className="profile__infoName">{profile.name ? profile.name : profile.login}</div>
          <div className="profile__infoCompany">{profile.company ? profile.company : 'No information'}</div>
        </div>
      </div>
    );
  
}

const Form = (props) => {
  const [userName, setUserName] = useState('');
  const handleSubmit = async (event) => {
    event.preventDefault();
    const resp = await 
      axios.get('https://api.github.com/users/' + userName.userName);
    props.onSubmit(resp.data);
    setUserName('');
  };
  return (
    <form onSubmit={handleSubmit} className="Form" action="">
      <input  
        value={props.userName}
        onChange={event => setUserName({ userName: event.target.value })} 
        type="text" 
        placeholer="GitHub Username"
        required 
      />
      <button>Add card</button>
    </form>
  );
  
}

const App = (props) => {
  const [profiles, setProfiles] = useState(testData);
  const addNewProfile = (profileData) => {
    // console.log('App', profileData)
    setProfiles([...profiles, profileData]);
  };
  return (
    <div className="App">
      <h1 className="header">{props.title}</h1>
      <Form onSubmit={addNewProfile}/>
      <CardList profiles={profiles} />
    </div>
  )
}

export default App;
